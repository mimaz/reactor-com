#include "rcom-core.h"

#include <string.h>
#include <stdio.h>

int
rcom_port_send (struct rcom_port *port,
		uint8_t code,
		uint8_t num_blocks,
		const size_t *size_v,
		const void **data_v)
{
    if (!port->serialize) {
	return -1;
    }

    return port->serialize (code, num_blocks, size_v, data_v,
			    port->write, port->write_closure,
			    port->serialize_closure);
}

int
rcom_port_feed (struct rcom_port *port,
		size_t size,
		const void *data)
{
    if (!port->deserialize) {
	return -1;
    }

    return port->deserialize (size, data,
			      port->parse, port->parse_closure,
			      port->deserialize_closure);
}

static uint8_t
crc_update(uint8_t crc, uint8_t data)
{
    uint8_t i;

    crc ^= data;

    for (i = 0; i < 8; i++) {
	if (crc & 0x80) {
	    crc = (crc << 1) ^ 0x07;
	} else {
	    crc = crc << 1;
	}
    }

    return crc;
}

void
rcom_serial_init (struct rcom_serial *serial,
		  struct rcom_port *port)
{
    memset (serial, 0, sizeof (*serial));

    if (port) {
	port->serialize = rcom_serialize;
	port->serialize_closure = serial;
	port->deserialize = rcom_deserialize;
	port->deserialize_closure = serial;
    }
}

void __rcom_serial_init () __attribute__ ((alias ("rcom_serial_init")));

int
rcom_serialize (uint8_t code,
		size_t num_blocks,
		const size_t *size_v,
		const void **data_v,
		rcom_write_t write,
		void *write_closure,
		void *unused)
{
    uint8_t header[6], fuse[2], size, crc, i, k;
    int written;

    if (!write) {
	return -1;
    }

    size = 0;

    for (i = 0; i < num_blocks; i++) {
	size += size_v[i];
    }

    crc = code;
    crc = crc_update (crc, size);

    header[0] = 0xff;
    header[1] = 0x00;
    header[2] = code;
    header[3] = size;
    header[4] = 0xaa;
    header[5] = crc;

    written = write (sizeof (header), header, write_closure);

    if (written < 0) {
	return written;
    }

    if (written != sizeof (header)) {
	return -1;
    }

    for (i = 0; i < num_blocks; i++) {
	written = write (size_v[i], data_v[i], write_closure);

	if (written < 0) {
	    return written;
	}

	if (written != size_v[i]) {
	    return -1;
	}

	for (k = 0; k < size_v[i]; k++) {
	    crc = crc_update (crc, ((const uint8_t *) data_v[i])[k]);
	}
    }

    fuse[0] = crc;
    fuse[1] = 0x55;

    written = write (sizeof (fuse), fuse, write_closure);

    if (written < 0) {
	return written;
    }

    if (written != sizeof (fuse)) {
	return -1;
    }

    return 0;
}

static void
deserialize_byte (struct rcom_serial *serial, uint8_t byte,
		  rcom_parse_t parse, void *parse_closure)
{
    serial->index++;

    switch (serial->index) {
    case 1:
	if (byte != 0xff) {
	    serial->index = 0;
	}
	break;

    case 2:
	if (byte != 0x00) {
	    serial->index = 0;
	}
	break;

    case 3:
	serial->code = byte;
	serial->crc = byte;
	break;

    case 4:
	serial->size = byte;
	serial->crc = crc_update (serial->crc, byte);
	break;

    case 5:
	if (byte != 0xaa) {
	    serial->index = 0;
	}
	break;

    case 6:
	if (byte != serial->crc) {
	    serial->index = 0;
	}
	break;

    case 7:
	serial->byteno = 0;
	/* fall through */

    default:
	if (serial->index < serial->size + 7) {
	    serial->data[serial->byteno++] = byte;
	    serial->crc = crc_update (serial->crc, byte);
	} else if (serial->index == serial->size + 7) {
	    if (byte != serial->crc) {
		serial->index = 0;
	    }
	} else if (byte != 0x55) {
	    serial->index = 0;
	} else {
	    serial->index = 0;
	    parse (serial->code, serial->byteno,
		   serial->data, parse_closure);
	}
    }
}

int
rcom_deserialize (size_t size,
		  const void *data,
		  rcom_parse_t parse,
		  void *parse_closure,
		  void *serial)
{
    size_t i;

    for (i = 0; i < size; i++) {
	deserialize_byte (serial, ((const uint8_t *) data)[i],
			  parse, parse_closure);
    }

    return 0;
}
