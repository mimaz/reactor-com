#include "bcom-ext.h"
#include "bcom.h"

#include <stdarg.h>
#include <stdio.h>

int
bcom_message_format (const char *fmt, ...)
{
    va_list args;
    int len;

    va_start (args, fmt);

    len = vsnprintf (NULL, 0, fmt, args);

    char buf[len + 1];

    vsnprintf (buf, len + 1, fmt, args);
    va_end (args);

    bcom_message (buf);

    return 0;
}
