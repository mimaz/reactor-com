#define bcom_curve_init(...) __bcom_curve_init(__VA_ARGS__)
#include "bcom-glib-curve.h"
#undef bcom_curve_init

struct _BComCurve
{
    RComStructure parent_instance;
};

G_DEFINE_TYPE (BComCurve, bcom_curve, RCOM_TYPE_STRUCTURE);

static void
bcom_curve_init (BComCurve *self)
{

}

static void
bcom_curve_class_init (BComCurveClass *cls)
{
}

BComCurve *
bcom_curve_new ()
{
    return g_object_new (BCOM_TYPE_CURVE,
			 "raw-size", sizeof (struct bcom_curve),
			 NULL);
}

/**
 * bcom_curve_new_wrap: (skip)
 */
BComCurve *
bcom_curve_new_wrap (struct bcom_curve *raw, gboolean copy)
{
    return g_object_new (BCOM_TYPE_CURVE,
			 "raw-pointer", raw,
			 "raw-copy", copy,
			 "raw-size", sizeof (struct bcom_curve),
			 NULL);
}
