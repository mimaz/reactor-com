#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define RCOM_TYPE_TARGET (rcom_target_get_type ())

G_DECLARE_INTERFACE (RComTarget, rcom_target,
		     RCOM, TARGET, GObject);

struct _RComTargetInterface
{
    GTypeInterface parent_iface;

    gint (*write_data) (RComTarget *self,
			guint size,
			gconstpointer data);
};

gint	rcom_target_write_data	    (RComTarget *self,
				     guint size,
				     gconstpointer data);
void	rcom_target_emit_feed	    (RComTarget *self,
				     guint size,
				     gconstpointer data);

G_END_DECLS
