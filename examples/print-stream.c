#include <bcom.h>
#include <stdio.h>

static int
write_data (size_t size, const void *data, void *user_data)
{
    size_t i;

    for (i = 0; i < size; i++) {
	printf ("%02x: %c\n",
		((const uint8_t *) data)[i], 
		((const uint8_t *) data)[i]);
    }

    return size;
}

int main ()
{
    struct rcom_serial serial0;
    struct bcom_port port0;

    bcom_port_init (&port0, NULL, write_data, NULL);
    rcom_serial_init (&serial0, &port0.base);

    bcom_port_message (&port0, "hello, rcom!");

    return 0;
}
