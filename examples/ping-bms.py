#!/usr/bin/python

import gi, signal

@gi-path@

gi.require_version('RCom', '1.0')
gi.require_version('BCom', '1.0')
gi.require_version('Gio', '2.0')

from gi.repository import RCom, BCom, Gio

port = BCom.Port.new()
port.set_target(RCom.Termios.new('/dev/ttyUSB0'))

serial = RCom.Serial.new()
serial.bind_port(port)

def on_activate(app):
    def on_sigint(sig, frame):
        app.release()

    signal.signal(signal.SIGINT, on_sigint)

    print('on_activate')

    def on_receive_ping(port, ack):
        print('ping ', ack)

        if ack > 0:
            print('pong ', ack - 1)
            port.send_ping(ack - 1)

        if ack < 2:
            app.release()

    port.connect('receive-ping', on_receive_ping)
    port.send_ping(100)

    app.hold()

app = Gio.Application.new('ping.bms', 0)
app.connect('activate', on_activate)
app.run(None)
