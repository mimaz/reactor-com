#include <bcom.h>
#include <stdio.h>

static int
write_data (size_t size, const void *data, void *user_data)
{
    /*
     * user_data is a pointer to bcom_port we want to communicate to
     * so when this function is called, it forward the stream to
     * the other port
     */

    bcom_port_feed (user_data, size, data);

    return size;
}

static void
receive_ping (struct bcom_port *port, uint32_t ack)
{
    /*
     * here we received a ping message
     */
    printf ("ping %u\n", ack);

    if (ack > 0) {
	fprintf (stderr, "pong %u\n", ack - 1);

	/*
	 * ping back so the sender will receive ack - 1
	 */
	bcom_port_ping (port, ack - 1);
    }
}

static struct bcom_receive receive = {
    .ping = receive_ping,
};

int main ()
{
    struct rcom_serial serial0, serial1;
    struct bcom_port port0, port1;

    /*
     * init port0,
     * first argument is a pointer to bcom_port structure,
     * then a pointer to bcom_receive structure,
     * then write callback and it's user data
     *
     * In this case the used data is port1 as we want to
     * ping back to port1, see write_data function
     */
    bcom_port_init (&port0, &receive, write_data, &port1);

    /*
     * the same for port1, here the user data is the port0
     */
    bcom_port_init (&port1, &receive, write_data, &port0);

    /*
     * init default serializators for port0 and port1
     */
    rcom_serial_init (&serial0, &port0.base);
    rcom_serial_init (&serial1, &port1.base);

    /*
     * send ping from port0 to port1 and see the result
     */
    bcom_port_ping (&port0, 100);

    return 0;
}
