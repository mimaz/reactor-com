#define rcom_serial_init(...) __rcom_serial_init (__VA_ARGS__)
#include "rcom-glib.h"
#undef rcom_serial_init

struct _RComSerial
{
    GObject parent_instance;
    struct rcom_serial serial;
};

G_DEFINE_TYPE (RComSerial, rcom_serial, G_TYPE_OBJECT);

static void
rcom_serial_init (RComSerial *self)
{
    __rcom_serial_init (&self->serial, NULL);
}

static void
rcom_serial_class_init (RComSerialClass *cls)
{

}

RComSerial *
rcom_serial_new ()
{
    return g_object_new (RCOM_TYPE_SERIAL, NULL);
}

static gint
serial_serialize (guint8 code,
		  size_t num_blocks,
		  const size_t *size_v,
		  const void **data_v,
		  RComWrite write,
		  gpointer write_closure,
		  gpointer closure)
{
    g_return_val_if_fail (RCOM_IS_SERIAL (closure), -1);

    return rcom_serialize (code, num_blocks, size_v, data_v,
			   write, write_closure,
			   &RCOM_SERIAL (closure)->serial);
}

static gint
serial_deserialize (gsize size,
		    gconstpointer data,
		    RComParse parse,
		    gpointer parse_closure,
		    gpointer closure)
{
    g_return_val_if_fail (RCOM_IS_SERIAL (closure), -1);

    return rcom_deserialize (size, data, parse, parse_closure,
			     &RCOM_SERIAL (closure)->serial);
}

void
rcom_serial_bind_port (RComSerial *self, RComPort *port)
{
    rcom_port_set_serialize (port, serial_serialize,
			     g_object_ref (self),
			     g_object_unref);
    rcom_port_set_deserialize (port, serial_deserialize,
			       g_object_ref (self),
			       g_object_unref);
}
