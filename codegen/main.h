#pragma once

#include <mkit.h>
#include <tree.h>

typedef enum _TypeTag TypeTag;
typedef struct _Options Options;
typedef struct _Type Type;
typedef struct _Value Value;
typedef struct _Enumeration Enumeration;
typedef struct _Field Field;
typedef struct _Structure Structure;
typedef struct _Arg Arg;
typedef struct _Message Message;
typedef struct _Protocol Protocol;

void generate_core (MkString **h_code, MkString **c_code,
		    Protocol *p, Options *o);
void generate_glib (MkString **h_code, MkString **c_code,
		    Protocol *p, Options *o);

enum _TypeTag
{
    TYPE_0,
    TYPE_C,
    TYPE_U8,
    TYPE_I8,
    TYPE_U16,
    TYPE_I16,
    TYPE_U32,
    TYPE_I32,
    TYPE_U64,
    TYPE_I64,
    TYPE_STRUCT,
};

struct _Options
{
    gchar *protocol;
    gchar *source;
    gchar *header;
    gchar *glib_source;
    gchar *glib_header;
};

void options_clear (Options *o);

G_DEFINE_AUTO_CLEANUP_CLEAR_FUNC (Options, options_clear);

struct _Type
{
    grefcount rc;
    TypeTag tag;
    Structure *s;
    MkString *stdc;
    MkString *glibc;
    MkString *gtype;
    MkString *gvalue;
    MkString *min;
    MkString *max;
};

Type *type_new (TypeTag tag, Protocol *p, Structure *s);
Type *type_ref (Type *t);
void type_unref (Type *t);

struct _Value
{
    grefcount rc;
    MkString *name;
    MkString *name_upper;
    guint value;
};

Value *value_new (MkString *name, guint value);
Value *value_ref (Value *v);
void value_unref (Value *v);

struct _Enumeration
{
    grefcount rc;
    Protocol *p;
    MkString *name;
    MkString *name_lower;
    MkString *name_upper;
    MkString *name_camel;
    MkArray *value_v;
};

Enumeration *enumeration_new (TreeEnum *te, Protocol *p, gboolean flags);
Enumeration *enumeration_ref (Enumeration *e);
void enumeration_unref (Enumeration *e);

struct _Field
{
    grefcount rc;
    MkString *name;
    MkString *name_lower;
    MkString *name_upper;
    Type *type;
};

Field *field_new (TreeField *tf, Protocol *p);
Field *field_ref (Field *f);
void field_unref (Field *f);

struct _Structure
{
    grefcount rc;
    gboolean ext;
    MkString *name;
    MkString *name_lower;
    MkString *name_upper;
    MkString *name_camel;
    MkString *prefix_lower;
    MkString *prefix_upper;
    MkArray *field_v;
    Type *type;
};

Structure *structure_new_from_tree (TreeStruct *ts, Protocol *p);
Structure *structure_ref (Structure *s);
void structure_unref (Structure *s);

struct _Arg
{
    grefcount rc;
    MkString *name;
    MkString *name_lower;
    MkString *length;
    Type *type;
};

Arg *arg_new_from_tree (TreeArg *ta, Protocol *p);
Arg *arg_ref (Arg *a);
void arg_unref (Arg *a);

struct _Message
{
    grefcount rc;
    MkString *name;
    MkString *name_lower;
    MkString *name_upper;
    MkArray *arg_v;
};

Message *message_new_from_tree (TreeMessage *tm, Protocol *p);
Message *message_ref (Message *m);
void message_unref (Message *m);

struct _Protocol
{
    grefcount rc;
    MkString *name;
    MkString *prefix_lower;
    MkString *prefix_upper;
    MkString *prefix_camel;
    MkArray *include_v;
    MkArray *glib_include_v;
    MkArray *enumeration_v;
    MkArray *structure_v;
    MkArray *message_v;
    Structure *port_struct;
    Type *port_type;
    GHashTable *type_t;
};

Protocol *protocol_new (TreeProtocol *tp);
Protocol *protocol_ref (Protocol *p);
void protocol_unref (Protocol *p);
Type *protocol_type_from_name (Protocol *p, MkString *name);
