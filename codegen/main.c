#include "main.h"

#include <tree.h>
#include <gio/gio.h>

G_DEFINE_AUTOPTR_CLEANUP_FUNC (Protocol, protocol_unref);

static void
write_file (const gchar *path, MkString *code)
{
    g_autoptr (GFile) file = NULL;
    g_autoptr (MkString) text = NULL;
    g_autoptr (GError) error = NULL;

    mk_code_format (code, &text, &error);

    if (error) {
	g_error ("%s", error->message);
    }

    file = g_file_new_for_path (path);

    g_file_replace_contents (file, text, mk_string_len (text),
			     NULL, FALSE,
			     G_FILE_CREATE_NONE,
			     NULL, NULL, &error);

    if (error) {
	g_error ("%s", error->message);
    }
}

gint
main (gint argc, gchar **argv)
{
    g_autoptr (GOptionContext) oc = NULL;
    g_autoptr (GError) error = NULL;
    g_autoptr (MkBuilder) builder = NULL;
    g_autoptr (MkCode) code = NULL;
    g_autoptr (TreeProtocol) pi = NULL;
    g_autoptr (Protocol) p = NULL;
    g_autoptr (MkString) h_code = NULL;
    g_autoptr (MkString) c_code = NULL;
    g_auto (Options) options = { NULL };

    tree_init_types ();

    GOptionEntry entries[] = {
	{ "protocol", 'p', 0, G_OPTION_ARG_FILENAME, &options.protocol,
	    "protocol definition file", "protocol.json" },
	{ "source", 's', 0, G_OPTION_ARG_FILENAME, &options.source,
	    "source file to generate", "protocol.c" },
	{ "header", 'h', 0, G_OPTION_ARG_FILENAME, &options.header,
	    "header file to generate", "protocol.h" },
	{ "glib-source", 'S', 0, G_OPTION_ARG_FILENAME, &options.glib_source,
	    "source file to generate", "protocol-glib.c" },
	{ "glib-header", 'H', 0, G_OPTION_ARG_FILENAME, &options.glib_header,
	    "header file to generate", "protocol-glib.h" },
	{ NULL },
    };

    oc = g_option_context_new (NULL);

    g_option_context_add_main_entries (oc, entries, NULL);
    g_option_context_parse (oc, &argc, &argv, &error);

    if (error) {
	goto finalize;
    }

    g_clear_pointer (&oc, g_option_context_free);

    builder = mk_builder_new ();
    pi = mk_builder_build_from_file (builder, options.protocol, "json",
				     TREE_TYPE_PROTOCOL, &error);

    if (error) {
	goto finalize;
    }

    g_clear_object (&builder);

    p = protocol_new (pi);

    g_clear_object (&pi);

    code = mk_code_new ();

    if (options.header || options.source) {
	generate_core (&h_code, &c_code, p, &options);

	if (options.header) {
	    write_file (options.header, h_code);
	}

	if (options.source) {
	    write_file (options.source, c_code);
	}
    }

    if (options.glib_header || options.glib_source) {
	h_code = mk_string_assign (h_code, "");
	c_code = mk_string_assign (c_code, "");

	generate_glib (&h_code, &c_code, p, &options);

	if (options.glib_header) {
	    write_file (options.glib_header, h_code);
	}

	if (options.glib_source) {
	    write_file (options.glib_source, c_code);
	}
    }

finalize:
    if (error) {
	g_error ("%s", error->message);
    }

    return 0;
}

void
options_clear (Options *o)
{
    g_free (o->protocol);
    g_free (o->source);
    g_free (o->header);
    g_free (o->glib_source);
    g_free (o->glib_header);
}

G_DEFINE_BOXED_TYPE (Type, type,
		     type_ref, type_unref);

Type *
type_new (TypeTag tag, Protocol *p, Structure *s)
{
    static struct {
	const gchar *stdc;
	const gchar *glibc;
	const gchar *gtype;
	const gchar *gvalue;
	const gchar *min;
	const gchar *max;
    } c_map[] = {
	[TYPE_C] = { "char", "gchar", "G_TYPE_CHAR", "char",
	    "SCHAR_MIN", "SCHAR_MAX" },
	[TYPE_U8] = { "uint8_t", "guint8", "G_TYPE_UINT", "uint",
	    "0", "G_MAXUINT8" },
	[TYPE_I8] = { "int8_t", "gint8", "G_TYPE_INT", "int",
	    "G_MININT8", "G_MAXINT8" },
	[TYPE_U16] = { "uint16_t", "guint16", "G_TYPE_UINT", "uint",
	    "0", "G_MAXUINT16" },
	[TYPE_I16] = { "int16_t", "gint16", "G_TYPE_INT", "int"
	    "G_MININT16", "G_MAXINT16" },
	[TYPE_U32] = { "uint32_t", "guint32", "G_TYPE_UINT", "uint"
	    "0", "G_MAXUINT32" },
	[TYPE_I32] = { "int32_t", "gint32", "G_TYPE_INT", "int"
	    "G_MININT32", "G_MAXINT32" },
	[TYPE_U64] = { "uint64_t", "guint64", "G_TYPE_UINT64", "uint64"
	    "0", "G_MAXUINT64" },
	[TYPE_I64] = { "int64_t", "gint64", "G_TYPE_INT64", "int64"
	    "G_MININT64", "G_MAXINT64" },
    };

    g_autoptr (MkString) name_lower = NULL;
    g_autoptr (MkString) name_upper = NULL;
    g_autoptr (MkString) name_camel = NULL;
    Type *t;

    t = g_new (Type, 1);

    g_ref_count_init (&t->rc);

    t->tag = tag;
    t->s = s;

    if (tag < TYPE_STRUCT) {
	t->stdc = mk_string_new_shared (c_map[tag].stdc);
	t->glibc = mk_string_new_shared (c_map[tag].glibc);
	t->gtype = mk_string_new_shared (c_map[tag].gtype);
	t->gvalue = mk_string_new_shared (c_map[tag].gvalue);
	t->min = mk_string_new_shared (c_map[tag].min);
	t->max = mk_string_new_shared (c_map[tag].max);
    } else {
	name_lower = mk_string_tolower (mk_string_ref (s->name));
	name_upper = mk_string_toupper (mk_string_ref (s->name));
	name_camel = mk_string_tocamel (mk_string_ref (s->name));

	t->stdc = mk_string_append (NULL, "struct %s_%s",
				    p->prefix_lower,
				    name_lower);
	t->glibc = mk_string_append (NULL, "%s%s",
				     p->prefix_camel,
				     name_camel);
	t->gtype = mk_string_append (NULL, "%s_TYPE_%s",
				     p->prefix_upper,
				     name_upper);
	t->gvalue = mk_string_new_shared ("object");
	t->min = NULL;
	t->max = NULL;

	g_hash_table_insert (p->type_t,
			     mk_string_ref (s->name),
			     type_ref (t));
    }

    return t;
}

Type *
type_ref (Type *t)
{
    g_ref_count_inc (&t->rc);

    return t;
}

void
type_unref (Type *t)
{
    if (g_ref_count_dec (&t->rc)) {
	mk_clear_string (&t->stdc);
	mk_clear_string (&t->glibc);
	mk_clear_string (&t->gtype);
	mk_clear_string (&t->gvalue);
	mk_clear_string (&t->min);
	mk_clear_string (&t->max);
	g_free (t);
    }
}

G_DEFINE_BOXED_TYPE (Value, value,
		     value_ref, value_unref);

Value *
value_new (MkString *name, guint value)
{
    Value *v;

    v = g_new (Value, 1);

    g_ref_count_init (&v->rc);

    v->name = mk_string_ref (name);
    v->name_upper = mk_string_toupper (mk_string_ref (name));
    v->value = value;

    return v;
}

Value *
value_ref (Value *v)
{
    g_ref_count_inc (&v->rc);

    return v;
}

void
value_unref (Value *v)
{
    if (g_ref_count_dec (&v->rc)) {
	mk_clear_string (&v->name);
	mk_clear_string (&v->name_upper);
	g_free (v);
    }
}

G_DEFINE_BOXED_TYPE (Enumeration, enumeration,
		     enumeration_ref, enumeration_unref);

Enumeration *
enumeration_new (TreeEnum *te, Protocol *p, gboolean flags)
{
    Enumeration *e;
    Value *v;
    guint value;

    e = g_new0 (Enumeration, 1);

    g_ref_count_init (&e->rc);

    e->p = p;
    e->name = mk_string_ref (tree_enum_get_name (te));
    e->name_lower = mk_string_tolower (mk_string_ref (e->name));
    e->name_upper = mk_string_toupper (mk_string_ref (e->name));
    e->name_camel = mk_string_tocamel (mk_string_ref (e->name));
    e->value_v = mk_array_new_type (value_get_type ());

    value = flags ? 1 : 0;

    TREE_ENUM_FOREACH_VALUE (te, tv) {
	v = value_new (tv, value);
	e->value_v = mk_array_append (e->value_v, v);
	value_unref (v);

	value = flags ? (value << 1) : (value + 1);
    }

    return e;
}

Enumeration *
enumeration_ref (Enumeration *e)
{
    g_ref_count_inc (&e->rc);

    return e;
}

void
enumeration_unref (Enumeration *e)
{
    if (g_ref_count_dec (&e->rc)) {
	mk_clear_string (&e->name);
	mk_clear_string (&e->name_lower);
	mk_clear_string (&e->name_upper);
	mk_clear_string (&e->name_camel);
	mk_clear_array (&e->value_v);
	g_free (e);
    }
}

G_DEFINE_BOXED_TYPE (Field, field,
		     field_ref, field_unref);

Field *
field_new (TreeField *tf, Protocol *p)
{
    Field *f;
    Type *t;

    f = g_new (Field, 1);
    t = protocol_type_from_name (p, tree_field_get_typex (tf));

    g_ref_count_init (&f->rc);

    f->name = mk_string_ref (tree_field_get_name (tf));
    f->name_lower = mk_string_tolower (mk_string_ref (f->name));
    f->name_upper = mk_string_toupper (mk_string_ref (f->name));
    f->type = t ? type_ref (t) : NULL;

    return f;
}

Field *
field_ref (Field *f)
{
    g_ref_count_inc (&f->rc);

    return f;
}

void
field_unref (Field *f)
{
    if (g_ref_count_dec (&f->rc)) {
	mk_clear_string (&f->name);
	mk_clear_string (&f->name_lower);
	mk_clear_string (&f->name_upper);
	g_clear_pointer (&f->type, type_unref);
	g_free (f);
    }
}

G_DEFINE_BOXED_TYPE (Structure, structure,
		     structure_ref, structure_unref);

Structure *
structure_new (MkString *name, gboolean ext, Protocol *p)
{
    Structure *s;

    s = g_new (Structure, 1);
    
    g_ref_count_init (&s->rc);

    s->ext = ext;
    s->name = mk_string_ref (name);
    s->name_lower = mk_string_tolower (mk_string_ref (name));
    s->name_upper = mk_string_toupper (mk_string_ref (name));
    s->name_camel = mk_string_tocamel (mk_string_ref (name));
    s->prefix_lower = mk_string_append (NULL, "%s_%s",
					p->prefix_lower,
					s->name_lower);
    s->prefix_upper = mk_string_append (NULL, "%s_%s",
					p->prefix_upper,
					s->name_upper);
    s->field_v = mk_array_new_type (field_get_type ());
    s->type = type_new (TYPE_STRUCT, p, s);

    return s;
}

Structure *
structure_new_from_tree (TreeStruct *ts, Protocol *p)
{
    Structure *s;
    Field *f;

    s = structure_new (tree_struct_get_name (ts),
		       tree_struct_get_extern (ts), p);
    
    TREE_STRUCT_FOREACH_FIELD (ts, tf) {
	f = field_new (tf, p);
	s->field_v = mk_array_append (s->field_v, f);
	field_unref (f);
    }

    return s;
}

Structure *
structure_ref (Structure *s)
{
    g_ref_count_inc (&s->rc);

    return s;
}

void
structure_unref (Structure *s)
{
    if (g_ref_count_dec (&s->rc)) {
	mk_clear_string (&s->name);
	mk_clear_string (&s->name_lower);
	mk_clear_string (&s->name_upper);
	mk_clear_string (&s->name_camel);
	mk_clear_string (&s->prefix_lower);
	mk_clear_string (&s->prefix_upper);
	mk_clear_array (&s->field_v);
	g_clear_pointer (&s->type, type_unref);
	g_free (s);
    }
}

G_DEFINE_BOXED_TYPE (Arg, arg,
		     arg_ref, arg_unref);

Arg *
arg_new_from_tree (TreeArg *ta, Protocol *p)
{
    Arg *a;

    a = g_new (Arg, 1);

    g_ref_count_init (&a->rc);

    a->name = mk_string_ref (tree_arg_get_name (ta));
    a->name_lower = mk_string_tolower (mk_string_ref (a->name));
    a->length = mk_string_ref (tree_arg_get_length (ta));
    a->type = type_ref (protocol_type_from_name (p, tree_arg_get_typex (ta)));

    return a;
}

Arg *
arg_ref (Arg *a)
{
    g_ref_count_inc (&a->rc);

    return a;
}

void
arg_unref (Arg *a)
{
    if (g_ref_count_dec (&a->rc)) {
	mk_clear_string (&a->name);
	mk_clear_string (&a->name_lower);
	mk_clear_string (&a->length);
	g_clear_pointer (&a->type, type_unref);
	g_free (a);
    }
}

G_DEFINE_BOXED_TYPE (Message, message,
		     message_ref, message_unref);

Message *
message_new_from_tree (TreeMessage *tm, Protocol *p)
{
    Message *m;
    Arg *a;

    m = g_new (Message, 1);

    g_ref_count_init (&m->rc);

    m->name = mk_string_ref (tree_message_get_name (tm));
    m->name_lower = mk_string_tolower (mk_string_ref (m->name));
    m->name_upper = mk_string_toupper (mk_string_ref (m->name));
    m->arg_v = mk_array_new_type (arg_get_type ());

    TREE_MESSAGE_FOREACH_ARG (tm, ta) {
	a = arg_new_from_tree (ta, p);

	m->arg_v = mk_array_append (m->arg_v, a);

	arg_unref (a);
    }

    return m;
}

Message *
message_ref (Message *m)
{
    g_ref_count_inc (&m->rc);

    return m;
}

void
message_unref (Message *m)
{
    if (g_ref_count_dec (&m->rc)) {
	mk_clear_string (&m->name);
	mk_clear_string (&m->name_lower);
	mk_clear_string (&m->name_upper);
	mk_clear_array (&m->arg_v);
	g_free (m);
    }
}

static void
register_scalar (Protocol *p, const gchar *name, TypeTag tag)
{
    g_hash_table_insert (p->type_t,
			 mk_string_new_shared (name),
			 type_new (tag, p, NULL));
}

Protocol *
protocol_new (TreeProtocol *tp)
{
    Protocol *p;
    Enumeration *e;
    Structure *s;
    Message *m;

    p = g_new (Protocol, 1);

    g_ref_count_init (&p->rc);

    p->name = mk_string_ref (tree_protocol_get_name (tp));
    p->prefix_lower = mk_string_tolower (mk_string_ref (p->name));
    p->prefix_upper = mk_string_toupper (mk_string_ref (p->name));
    p->prefix_camel = mk_string_del_char (mk_string_ref (p->name), '-');
    p->include_v = mk_array_new_type (MK_TYPE_STRING);
    p->glib_include_v = mk_array_new_type (MK_TYPE_STRING);
    p->enumeration_v = mk_array_new_type (enumeration_get_type ());
    p->structure_v = mk_array_new_type (structure_get_type ());
    p->message_v = mk_array_new_type (message_get_type ());
    p->type_t = g_hash_table_new_full (g_str_hash,
				       g_str_equal,
				       (GDestroyNotify)
				       mk_string_unref,
				       (GDestroyNotify)
				       type_unref);

    p->port_struct = structure_new (mk_string_newa ("port"), TRUE, p);
    p->port_type = type_new (TYPE_STRUCT, p, p->port_struct);

    register_scalar (p, "c", TYPE_C);
    register_scalar (p, "i8", TYPE_I8);
    register_scalar (p, "u8", TYPE_U8);
    register_scalar (p, "i16", TYPE_I16);
    register_scalar (p, "u16", TYPE_U16);
    register_scalar (p, "i32", TYPE_I32);
    register_scalar (p, "u32", TYPE_U32);
    register_scalar (p, "i64", TYPE_I64);
    register_scalar (p, "u64", TYPE_U64);

    TREE_PROTOCOL_FOREACH_INCLUDE (tp, ti) {
	p->include_v = mk_array_append (p->include_v, ti);
    }

    TREE_PROTOCOL_FOREACH_GLIB_INCLUDE (tp, ti) {
	p->glib_include_v = mk_array_append (p->glib_include_v, ti);
    }

    TREE_PROTOCOL_FOREACH_FLAG (tp, te) {
	e = enumeration_new (te, p, TRUE);
	p->enumeration_v = mk_array_append (p->enumeration_v, e);
	enumeration_unref (e);
    }

    TREE_PROTOCOL_FOREACH_ENUM (tp, te) {
	e = enumeration_new (te, p, TRUE);
	p->enumeration_v = mk_array_append (p->enumeration_v, e);
	enumeration_unref (e);
    }

    TREE_PROTOCOL_FOREACH_STRUCT (tp, ts) {
	s = structure_new_from_tree (ts, p);
	p->structure_v = mk_array_append (p->structure_v, s);
	structure_unref (s);
    }

    TREE_PROTOCOL_FOREACH_MESSAGE (tp, tm) {
	m = message_new_from_tree (tm, p);
	p->message_v = mk_array_append (p->message_v, m);
	message_unref (m);
    }

    return p;
}

Protocol *
protocol_ref (Protocol *p)
{
    g_ref_count_inc (&p->rc);

    return p;
}

void
protocol_unref (Protocol *p)
{
    if (g_ref_count_dec (&p->rc)) {
	mk_clear_string (&p->name);
	mk_clear_string (&p->prefix_lower);
	mk_clear_string (&p->prefix_upper);
	mk_clear_string (&p->prefix_camel);
	mk_clear_array (&p->include_v);
	mk_clear_array (&p->glib_include_v);
	mk_clear_array (&p->enumeration_v);
	mk_clear_array (&p->structure_v);
	mk_clear_array (&p->message_v);
	g_clear_pointer (&p->type_t, g_hash_table_unref);
	g_clear_pointer (&p->port_struct, structure_unref);
	g_clear_pointer (&p->port_type, type_unref);
	g_free (p);
    }
}

Type *
protocol_type_from_name (Protocol *p, MkString *name)
{
    Type *t = g_hash_table_lookup (p->type_t, name);

    if (!t) {
	g_error ("no such type: %s", name);
    }

    return t;
}
