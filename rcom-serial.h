#pragma once

#include "rcom-port.h"

G_BEGIN_DECLS

#define RCOM_TYPE_SERIAL (rcom_serial_get_type ())

G_DECLARE_FINAL_TYPE (RComSerial, rcom_serial,
		      RCOM, SERIAL, GObject);

RComSerial *rcom_serial_new		(void);
void	    rcom_serial_bind_port	(RComSerial *self,
					 RComPort *port);

G_END_DECLS
