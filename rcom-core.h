#pragma once

#include <stdint.h>
#include <stddef.h>

typedef int (*rcom_write_t) (size_t size,
			     const void *data,
			     void *closure);

typedef int (*rcom_parse_t) (uint8_t code,
			     size_t size,
			     const void *data,
			     void *closure);

typedef int (*rcom_serialize_t) (uint8_t code,
				 size_t num_blocks,
				 const size_t *size_v,
				 const void **data_v,
				 rcom_write_t write,
				 void *write_closure,
				 void *closure);

typedef int (*rcom_deserialize_t) (size_t size,
				   const void *data,
				   rcom_parse_t parse,
				   void *parse_closure,
				   void *closure);

struct rcom_port
{
    rcom_serialize_t serialize;
    void *serialize_closure;

    rcom_write_t write;
    void *write_closure;

    rcom_deserialize_t deserialize;
    void *deserialize_closure;

    rcom_parse_t parse;
    void *parse_closure;

    void *context;
};

struct rcom_serial
{
    uint16_t index;
    uint8_t code;
    uint8_t crc;
    uint8_t size;
    uint8_t byteno;
    uint8_t data[256];
};

int rcom_port_send (struct rcom_port *port,
		    uint8_t code,
		    uint8_t num_blocks,
		    const size_t *size_v,
		    const void **data_v);

int rcom_port_feed (struct rcom_port *port,
		    size_t size,
		    const void *data);

void rcom_serial_init (struct rcom_serial *serial,
		       struct rcom_port *port);

int rcom_serialize (uint8_t code,
		    size_t num_blocks,
		    const size_t *size_v,
		    const void **data_v,
		    rcom_write_t write,
		    void *write_closure,
		    void *unused);

int rcom_deserialize (size_t size,
		      const void *data,
		      rcom_parse_t parse,
		      void *parse_closure,
		      void *serial);
